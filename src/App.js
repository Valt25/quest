import React from 'react';
import './App.css';
import Skip from "./components/skip/Skip";
import TaskWrapper from "./components/task/TaskWrapper";
import {Route, Switch} from "react-router-dom";
import 'bootstrap/dist/css/bootstrap.min.css';

function App() {
  return (
      <Switch>
        <Route path='/skip' component={Skip}/>
        <Route path='/task/:hash' component={TaskWrapper}/>
        <Route path='/404'>
            <h2 className='text-center'>Данной страницы не существует</h2>
        </Route>
      </Switch>
  );
}

export default App;
