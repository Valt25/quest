import React, {useState} from "react";
import {useParams, useHistory} from "react-router-dom"
import {tasks} from "../../tasks";
import TaskDescription from "./TaskDescription";
import ToSkipButton from "../skip/ToSkipButton";
import Password from "../Password";
export default function () {
    const taskId = useParams().hash;
    const currentTask = tasks[taskId];
    if (!currentTask) {
        useHistory().push('/404');
        return null;
    }
    let [permitted, setPermitted] = useState(false);
    const nextTask = tasks[currentTask.nextTask];
    return permitted ?<div>
        <TaskDescription currentTask={currentTask} nextTask={nextTask}/>
        <ToSkipButton/>
    </div>: <Password onSubmitCallback={() => setPermitted(true)} expectedPassword={currentTask.code} isFullScreen={true}/>
}