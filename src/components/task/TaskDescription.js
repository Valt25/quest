import React from "react";

export default function (props) {
    const {currentTask, nextTask} = props;
    return <div className='text-center'>
        <h3>{currentTask.text} </h3>
        {
            nextTask ? <h4>Код для следующего задания: {nextTask.code}</h4> : ''
        }
    </div>

}