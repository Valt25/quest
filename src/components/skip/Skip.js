import React, {useState} from "react";
import './skip.css'
import Result from "../Result";
import Password from "../Password";

export default function () {
    const [showResult, setShowResult] = useState(false);
    const [time, setTime] = useState();

    if (!time) {
        fetch('http://worldtimeapi.org/api/timezone/Europe/Moscow')
            .then(res => res.json())
            .then((data) => {
                const currentDate = new Date(data.datetime);
                setTime(currentDate);
                setShowResult(currentDate.getTime() > new Date(2019, 10, 5, 13).getTime())

            })
            .catch(console.log)
    }
    return (<div className='h-100 d-flex align-items-center justify-content-around'>
        <Result showResult={showResult}/>
        { !showResult && <div className='w-75'><h2 className='text-center'>Чтобы пропустить все этапы, и сразу получить приз. Вы
            можете:</h2>
            <ol>
                <li>
                    Подождать нужного времени. И за 2 часа до моего приезда. На данной странице появится нужная
                    информация.
                </li>
                <li>
                    Ввести секретный код, который можно узнать только у меня. Я думаю вы знаете, что нужно сделать чтобы
                    его получить.
                </li>
            </ol>
            <Password onSubmitCallback={() => setShowResult(true)} expectedPassword='You want me' isFullScreen={false}/>
        </div>
        }
    </div>)
}