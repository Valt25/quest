import React from "react";
import Button from "react-bootstrap/Button";
import './skip.css'
import {Link} from "react-router-dom";

export default function () {
    return (<Link to='/skip'><Button className='position-fixed rounded rounded-circle skip-button'>skip</Button></Link>)
}