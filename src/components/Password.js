import React, {useState} from "react";
import Button from "react-bootstrap/Button";

export default function Password(props) {
    let {onSubmitCallback, isFullScreen, expectedPassword} = props;
    const [inputValue, setValue] = useState('');
    const updateValue = (e) => {
        setValue(e.target.value)
    };
    const [message, setMessage] = useState('');
    let fullScreenWrapper = (inner) => <div className='position-fixed h-100 w-100 bg-white  d-flex flex-column justify-content-around align-items-center'>{inner}</div>;
    let onSubmit = () => {
        if (inputValue === expectedPassword) {
            onSubmitCallback();
        } else {
            setMessage('Неправильный код')
        }
    };
    let password = <div>
        <p>Введите код. Это может быть код предыдущего задания, или специально выданный вам код, администратором квеста.</p>
        <input value={inputValue} onChange={updateValue}/> <Button onClick={onSubmit}>Ок</Button><span>{message}</span>
    </div>;
    return isFullScreen ? fullScreenWrapper(password): password
}