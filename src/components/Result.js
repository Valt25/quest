import React, {useState} from "react";

export default function (props) {
    const {showResult} = props;
    const originalTime = new Date(2019, 11, 6);
    const [currentTime, setTime] = useState(originalTime.getTime() + Math.floor(Math.random() * 10000) - new Date().getTime() );
    setTimeout(() => setTime(currentTime - 1000), 1000);
    return <div>
        {showResult ? <img src={process.env.PUBLIC_URL + '/result.jpg'} alt='Ответ'/> : <p>{~~(currentTime/1000)}</p>}
    </div>

}