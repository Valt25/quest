export const tasks = {
    'b1f6d5a6d7eebb3f5bdcfb357488033b': {
        text: 'Это одна из немногих наших общих знакомых, которая нравится мне как девушка. Но скорее всего безответно',
        code: 'Secret',
        nextTask: '0c9e1ceab3fbcce9c22e9f681da11aae'
    },
    '0c9e1ceab3fbcce9c22e9f681da11aae': {
        text: 'Какого числа мы познакомились, введите день года, в адрес сайта после /task/',
        code: 'OneMoreSecret',
        nextTask: '149'
    },
    '149': {
        text: 'Если наши родители будут против свадьбы и мы сбежим, мать этого человека найдет нас первым.',
        code: 'Cbcrb{jxeCbcrb',
        nextTask: 'b9f0861aa8636315c2f3f53a5d51315a'
    },
    'b9f0861aa8636315c2f3f53a5d51315a': {
        text: 'Парк где я в первые пытался трогать твою грудь. Найди там книги и пролистай их',
        code: 'asfgjrnd',
        nextTask: 'f819017078a24083de554d4c26b47ef4'
    },
        'f819017078a24083de554d4c26b47ef4': {
        text: 'Место где с нами произошла одна из самых смешных ситуаций в начале нашей дружбы, нудно найти там наклееный qr код',
        code: 'OhMyGrandma',
        nextTask: 'preresult'
    },
    'preresult': {
        text: 'Это было последнее задание, нажми кнопку skip и введи этот код на той странице.',
        code: '1230udsf-w3jg8',
        nextTask: 'result'
    },
    'result': {
        text: '',
        code: 'You want me',
        nextTask: null
    },

};